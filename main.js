const botaoInicial = document.getElementById("play")
const modalInicial = document.getElementById("modal-inicio")
const main = document.getElementById("main")
const musicaJogo = document.getElementById("musica-jogo")
const semAudio = document.getElementById("audio")
const imagemVolume = document.getElementById("volume-img")
const botaoRegras = document.getElementById("regras")
const modalRegras = document.getElementById("modal-regras")

botaoInicial.addEventListener("click", iniciarJogo)

semAudio.addEventListener("click", semSom)
musicaJogo.volume = 0.3;

botaoRegras.addEventListener("click", mostraRegras)


function iniciarJogo() {
    modalInicial.style.display = "none"
    main.style.display = "block"
    game()
}

function mostraRegras() {
    let regrasImagem = document.getElementById("regras-img")
    if(modalRegras.className === "modal-fechado") {
        modalRegras.className = "modal-aberto"
        regrasImagem.setAttribute("src", "assets/icons/rules-closed.png")
    } else {
        modalRegras.className = "modal-fechado"
        regrasImagem.setAttribute("src", "assets/icons/rules.png")
    }
}

function semSom() {
    if(musicaJogo.muted === false) {
        imagemVolume.setAttribute("src", "assets/icons/volume-on.png")
        musicaJogo.muted = true;
    } else {
        imagemVolume.setAttribute("src", "assets/icons/volume-off.png")
        musicaJogo.muted = false
    }
}